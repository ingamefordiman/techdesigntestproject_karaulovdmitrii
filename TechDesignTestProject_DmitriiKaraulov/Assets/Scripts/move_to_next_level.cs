using UnityEngine;
using UnityEngine.SceneManagement;

public class move_to_next_level : MonoBehaviour
{
    [SerializeField] private GameObject buttonNextLevel;
    [SerializeField] private int sceneIndex;

    public void OnMouseDown()
    {
        buttonNextLevel.SetActive(true);
    }

    public void MoveNextLevel()
    {
        SceneManager.LoadScene(sceneIndex);
    }
}
